//
//  ViewController.m
//  SocketsEg
//
//  Created by Shaun Persad on 5/16/15.
//  Copyright (c) 2015 domandtom. All rights reserved.
//

#import "ViewController.h"
#import "SocketsEg-Swift.h"
#import "AFHTTPRequestOperationManager.h"


@interface ViewController (){
    NSMutableDictionary *queued_metas; // keeps meta objects who don't yet have an artist.
    NSMutableDictionary *queued_artists; // keeps artists who don't yet have a meta object.
    NSMutableArray *search_results; // the combined artists + meta to display.
    NSString *access_token; // same access_token from your other calls.
    NSString *api_url;
    NSString *search_endpoint;
    SocketIOClient* socket;
    BOOL use_sockets; // switch between getting the meta with the http response, or deferring to sockets.
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSLog(@"view loaded");
    
    use_sockets = YES;

    queued_metas = [[NSMutableDictionary alloc] init];
    queued_artists = [[NSMutableDictionary alloc] init];
    search_results = [[NSMutableArray alloc] init];
    
    api_url = @"http://lisa-api.dev:1337";
    search_endpoint = [NSString stringWithFormat:@"%@/artists/search", api_url];
    access_token = @"jKAhIAp663dXAxHUC4s0zdnG1k2ejHuprjpjfFadz0NDlYTRb2wb9P9940jxXJBu7fENsHthI7wpzWAY9OiToyHwykSkyv5NnMDd";
    
    if (use_sockets) {
     
        
        NSDictionary *options = @{
                                  @"connectParams": @{
                                          @"__sails_io_sdk_version": @"1.3.5",
                                          @"access_token": access_token
                                          }
                                  };
        
        /**
         * Set up socket events.
         */
        socket = [[SocketIOClient alloc] initWithSocketURL:api_url options:options];
        
        /*
         * Will fire whenever the socket connects.
         * Calling "search" is done in here because the socket needs to be connected at the time of the
         * http API request to /search so that it can simulataneously deliver the meta.
         */
        [socket on:@"sails_connect" callback:^(NSArray* data, void (^ack)(NSArray*)) {
            NSLog(@"socket connected");
            [self search];
        }];
        [socket on:@"error" callback:^(NSArray* data, void (^ack)(NSArray*)) {
            NSLog(@"%@", data);
        }];
        [socket on:@"disconnect" callback:^(NSArray* data, void (^ack)(NSArray*)) {
            NSLog(@"socket disconnected");
        }];
        /**
         * This event fires after the http API request to /search.
         * It will fire for each artist in the search results and deliver 1 meta object each.
         */
        [socket on:@"artist_meta" callback:^(NSArray* data, void (^ack)(NSArray*)) {
            
            NSLog(@"got a meta object");
            
            NSDictionary *meta = [data objectAtIndex:0];
            
            NSString *key = [NSString stringWithFormat:@"artist_%@", [meta objectForKey:@"artist_id"]];
            
            if ([queued_artists objectForKey:key]) { // if there are artists waiting for this meta...
                
                NSMutableDictionary *mutable_artist = [queued_artists objectForKey:key]; // get the waiting artist.
                [mutable_artist setObject:meta forKey:@"meta"]; // set their meta.
                [queued_artists removeObjectForKey:key]; // remove the artist from the queue.
                
            } else { // if not, queue the meta to wait for their artist.
                
                [queued_metas setObject:meta forKey:key]; // associate the meta with an artist id.
            }
            [self checkForCompletedSearchResults]; // check if all the artists have their meta.
            
        }];
        
        [socket connect];
        
    } else {
        [self search];
    }
    
}

/**
 * The http API request
 */
- (void) search {
    
    [search_results removeAllObjects];
    [queued_artists removeAllObjects];
    
    NSDictionary *params = @{
                             @"access_token":access_token,
                             @"lat": [NSNumber numberWithFloat:40.7048],
                             @"lng": [NSNumber numberWithFloat:-74.0123739],
                             @"search_radius": [NSNumber numberWithInt:50000],
                             @"when": @"now",
                             @"where": @"consumers_place",
                             @"price_range": [NSNumber numberWithInt:2],
                             @"service_category_id": [NSNumber numberWithInt:1],
                             @"use_sockets": @(use_sockets)
                             };
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    [manager GET:search_endpoint parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"got search results");
        
        for (NSDictionary *artist in responseObject) {

            NSMutableDictionary *mutable_artist = [NSMutableDictionary dictionaryWithDictionary:artist];
            
            [search_results addObject:mutable_artist];
            
            if (use_sockets) {
                
                NSString *key = [NSString stringWithFormat:@"artist_%@", [artist objectForKey:@"id"]];
                
                
                if ([queued_metas objectForKey:key]) { // if there is a meta object waiting for this artist...
                    
                    NSDictionary *meta = [queued_metas objectForKey:key]; // get the waiting meta.
                    
                    [mutable_artist setObject:meta forKey:@"meta"]; // set their meta.
                    // note, unlike in the meta event,
                    // we don't want to remove meta objects from
                    // the queued_metas dictionary.
                    // This will help with future similar requests,
                    // since you won't have to wait for the meta to populate.
                    // (if any artist id's are the same as your last requests, you'll already
                    //  have their meta data and so you can display them right away!)
                    
                } else { // if not, queue the artist to wait for their meta.
                    
                    [queued_artists setObject:mutable_artist forKey:key];
                }
                
            }
        }
        [self checkForCompletedSearchResults]; // check if all the artists have their meta.
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
    
    
}

- (void) checkForCompletedSearchResults {
    
    if (![queued_artists count]) {
        
        NSLog(@"You can display the completed results now.");
        NSLog(@"%@", search_results);
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
