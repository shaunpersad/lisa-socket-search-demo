//
//  AppDelegate.h
//  SocketsEg
//
//  Created by Shaun Persad on 5/16/15.
//  Copyright (c) 2015 domandtom. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

